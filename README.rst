======
mocko3
======


.. image:: https://img.shields.io/pypi/v/mocko3.svg
        :target: https://pypi.python.org/pypi/mocko3

.. image:: https://img.shields.io/travis/dawiediamond/mocko3.svg
        :target: https://travis-ci.org/dawiediamond/mocko3

.. image:: https://readthedocs.org/projects/mocko3/badge/?version=latest
        :target: https://mocko3.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Python module to create mocking function for AWS services.


* Free software: MIT license
* Documentation: https://mocko3.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
