#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `mocko3` package."""


import unittest

from mocko3 import mocko3


class TestMocko3(unittest.TestCase):
    """Tests for `mocko3` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_000_something(self):
        """Test something."""
