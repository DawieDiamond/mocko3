import os
import shutil
import datetime

class StreamingBody:
    def __init__(self, body):
        self.body = body
    def read(self):
        return self.body

class mockoS3:
    def __init__(self, **kwargs):
        self._location = '../S3'
        # Create the S3 file if it does not exist.
        if not os.path.exists(self._location):
            os.mkdir(self._location)
    def setS3Location(self, new_location):
        self._location = new_location
        # Create the S3 file if it does not exist.
        if not os.path.exists(self._location):
            os.mkdir(self._location)
    def upload_file(self, *,Filename, Bucket, Key):
        ''' This function uploads a file given in Filename in posts it to the MockoS3 
        bucket called Bucket using Key.
        '''
        if not os.path.exists(Filename):
            raise ValueError(f'The file {Filename} does not exist')

        # Create bucket if it does not exist
        if not self.bucketExists(Bucket=Bucket):
            raise ValueError(f'The Bucket {Bucket} does not exist.')
        try:
            try:
                os.remove(os.path.join(self._location, Bucket, Key))
            except:
                pass
            shutil.copy2(Filename, os.path.join(self._location, Bucket, Key))
        except shutil.SameFileError:
            # code when Exception occur
            print('IS THE SAME FILE, NOT COPYING')
        else:
            # code if the exception does not occur
            pass
        finally:
            # code which is executed always
            pass
    def create_bucket(self, *,Bucket):
        if self.bucketExists(Bucket=Bucket):
            raise ValueError(f'The bucket {Bucket} already exists')
        os.mkdir(os.path.join(self._location, Bucket))
    def bucketExists(self, *, Bucket):
        if not os.path.exists(os.path.join(self._location, Bucket)):
            return False
        return True
    def KeyExists(self, *,Bucket, Key):
        if not os.path.exists(os.path.join(self._location, Bucket, Key)):
            return False
        return True
    def list_buckets(self):
        ''' A function used to list all buckets for this group.
        '''
        response_object = {'ResponseMetadata': {'RequestId': 'None',
        'HostId': 'localhost',
        'HTTPStatusCode': 200,
        'HTTPHeaders': {'x-amz-id-2': 'None',
        'x-amz-request-id': 'None',
        'date': datetime.datetime.today().isoformat(),
        'content-type': 'application/xml',
        'transfer-encoding': 'chunked',
        'server': 'Mocko3'},
        'RetryAttempts': 0},
        'Owner': {'DisplayName': 'None',
        'ID': 'None'}}
        # Get all files in your local bucket
        files = os.listdir(self._location)
        for fb in files:
            if not 'Buckets' in response_object: response_object['Buckets'] = []
            response_object['Buckets'].append({
                'Name':fb,
                'CreationDate': 'None'
            })
        return response_object
    def list_objects(self, *, Bucket):
        ''' A function used to list all objectsin a Bucket.
        '''
        if not self.bucketExists(Bucket=Bucket):
            raise ValueError(f'The bucket {Bucket} does not exist.')

        response_object = {'ResponseMetadata': {'RequestId': 'None',
                            'HostId': 'localhost',
                            'HTTPStatusCode': 200,
                            'HTTPHeaders': {'x-amz-id-2': 'None',
                            'x-amz-request-id': 'None',
                            'date': datetime.datetime.today().isoformat(),
                            'x-amz-bucket-region': 'None',
                            'content-type': 'application/xml',
                            'transfer-encoding': 'chunked',
                            'server': 'Mocko3'},
                            'RetryAttempts': 0},
                            'IsTruncated': False,
                            'Marker': '',
                            'Name': Bucket,
                            'Prefix': '',
                            'MaxKeys': 1000,
                            'EncodingType': 'url'}

        for fb in os.listdir( os.path.join(self._location, Bucket)):
            if not 'Contents' in response_object: response_object['Contents'] = []
            response_object['Contents'].append({
                    'Key': fb,
                    'LastModified': 'None',
                    'ETag': 'None',
                    'Size': None,
                    'StorageClass': 'STANDARD',
                    'Owner': {'DisplayName': 'None',
                    'ID': 'None'}
                    })
        return response_object
    def get_object(self, *,Bucket,Key):
        ''' A function used to get an object from an S3 bucket.
        '''
        response_object = {'ResponseMetadata': {'RequestId': 'None',
              'HostId': 'localhost',
              'HTTPStatusCode': 200,
              'HTTPHeaders': {'x-amz-id-2': 'None',
               'x-amz-request-id': 'None',
               'date': datetime.datetime.today().isoformat(),
               'last-modified': 'Thu, 16 May 2019 16:59:54 GMT',
               'etag': '"None"',
               'accept-ranges': 'bytes',
               'content-type': 'binary/octet-stream',
               'content-length': 'None',
               'server': 'Local'},
              'RetryAttempts': 0},
             'AcceptRanges': 'bytes',
             'LastModified': 'None',
             'ContentLength': 'None',
             'ETag': 'None',
             'ContentType': 'binary/octet-stream',
             'Metadata': {},
             'Body': None}
        if not self.bucketExists(Bucket=Bucket):
            raise ValueError(f'The Bucket {Bucket} does not exist.')
        if not self.KeyExists(Bucket=Bucket, Key=Key):
            raise ValueError(f'The Key {Key} does not exist in the Bucket {Bucket}.')
        with open(os.path.join(self._location, Bucket, Key), 'rb') as fb:
            return_object = StreamingBody(fb.read())
        response_object['Body'] = return_object
        return response_object