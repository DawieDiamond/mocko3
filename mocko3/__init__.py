# -*- coding: utf-8 -*-

"""Top-level package for mocko3."""

__author__ = """David Hercules Diamond"""
__email__ = 'dawie@invokeanalytics.co.za'
__version__ = '0.1.0'

from mocko3 import *