# -*- coding: utf-8 -*-

"""Main module."""
from .mockos3 import mockoS3

def client(**kwargs):
    ''' This function mimics boto3.client. It creates clients to certain services.
    '''
    missing_vals = list(set(['service_name', 'region_name']) - set(kwargs.keys()))
    if missing_vals:
        raise ValueError(f'The following keywords are missing: {missing_vals}')
    _available_names = ['s3']
    if kwargs['service_name'] not in _available_names:
        raise ValueError(f'The product {kwargs["service_name"]} is not available in mocko3')
    product_class_dict = {
        's3' : mockoS3
    }
    return product_class_dict[kwargs['service_name']](**kwargs)

if __name__ == '__main__':
    AWS_ACCESS_KEY_ID = 'dasdsa'
    SECRET_ACCESS_KEY = '456456'
    s3 = client(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                aws_secret_access_key=SECRET_ACCESS_KEY,
                                service_name='s3',
                                region_name='us-east-1')
